import subprocess
import re
import os

def get_stdout(cmd):
    print(cmd)
    os.system(cmd)
	print("\n")
    #cmd = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)  
    #data = cmd.stdout.read()
    #cmd.terminate()
    #print(data)
    return 0

def to_string(unknown):
    result = ""
    for sign in unknown:
        if sign != "[" and sign != "]" and sign != "'" and sign != " ":
            result += sign
    return result

    
def main():
    cli = "python3 Cli.py "
    add_job = cli + "add-job script-job"
    add_job1 = cli + "add-job script-job1"
    add_job2 = cli + "add-job script-job2"
    add_job3 = cli + "add-job script-job3"
    add_job4 = cli + "add-job script-job4"
    add_job5 = cli + "add-job script-job5"
    get_jobs = cli + "get-jobs"
    get_job = cli + "get-job "
    get_job_log = cli + "get-job-log "
    get_logentry = cli + "getlogentry "
    delete_job = cli + "delete-job "
    
    add_operation = cli + "add-op "
    operation = " COPY "
    op_target = " server2:/home "
    op_source = " server2:/boot "
    get_operations = cli + "get-operations "
    update_op = cli + "update-operation "
    get_operation = cli + "get-operation "
    delete_operation = cli + "delete-operation "
    finalize_job = cli + "finalize-job "
    get_status = cli + "get-status "
    
    get_servers = cli + "get-servers "
    update_server = cli + "update-server "
    server_id = "1 "
    server_hostname = " testserver:/ "
    capacity = " 3333 "
    get_server = cli + "get-server "
    ls = cli + "ls "
    depth = " 2 "
    
    
    """create, update, show and delete jobs
    
    """
    # add-job
    data = get_stdout(add_job)
    data = get_stdout(add_job1)
    data = get_stdout(add_job2)
    data = get_stdout(add_job3)
    data = get_stdout(add_job4)
    data = get_stdout(add_job5)
    
    # get-jobs
    data = get_stdout(get_jobs)
    
    # get-logentry
    data = get_stdout(get_logentry) 

    
    """create, update, show and delete operations
    
    """
    # add-job
    data = get_stdout(add_job)
    data = get_stdout(add_job1)
    data = get_stdout(add_job2)
    data = get_stdout(add_job3)
    data = get_stdout(add_job4)
    data = get_stdout(add_job5)
   
   
    """update and show server and show server-directories
    
    """
    # get-servers
    data = get_stdout(get_servers)
    
    # ls
    data = get_stdout(ls + server_hostname)
    data = get_stdout(ls + op_target)
    data = get_stdout(ls + "test7:/ 3")
    
   
    return 0

    
    
if __name__ == '__main__':
    if main() == 1:
        print("An error has occurred during the test!")
